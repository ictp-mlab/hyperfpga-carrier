# This constraint file was automatically generated 
 #based on the pinout Excel File. It is only valid for 4EG. 

#----- East Connector -----
# Carrier signal name B228_TX3_N
#set_property PACKAGE_PIN N3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_TX3_P
#set_property PACKAGE_PIN N4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_RX3_N
#set_property PACKAGE_PIN P1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_RX3_P
#set_property PACKAGE_PIN P2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_TX3_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_TX3_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_RX3_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_RX3_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_TX3_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_TX3_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_RX3_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_RX3_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_TX3_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_TX3_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_RX3_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_RX3_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L17_N
#set_property PACKAGE_PIN A8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L17_P
#set_property PACKAGE_PIN A9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L2_N
#set_property PACKAGE_PIN A6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L2_P
#set_property PACKAGE_PIN A7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L3_N
#set_property PACKAGE_PIN A4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L3_P
#set_property PACKAGE_PIN B4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L21_N
#set_property PACKAGE_PIN A5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L21_P
#set_property PACKAGE_PIN B5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L7_N
#set_property PACKAGE_PIN A1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L7_P
#set_property PACKAGE_PIN A2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L11_N
#set_property PACKAGE_PIN C4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L11_P
#set_property PACKAGE_PIN D4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L4_N
#set_property PACKAGE_PIN A14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L4_P
#set_property PACKAGE_PIN B14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L1_N
#set_property PACKAGE_PIN A13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L1_P
#set_property PACKAGE_PIN B13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L19_N
#set_property PACKAGE_PIN D9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L19_P
#set_property PACKAGE_PIN E9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L22_N
#set_property PACKAGE_PIN B6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L22_P
#set_property PACKAGE_PIN C6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L20_N
#set_property PACKAGE_PIN B8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L20_P
#set_property PACKAGE_PIN C8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L16_N
#set_property PACKAGE_PIN E8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L16_P
#set_property PACKAGE_PIN F8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L12_N
#set_property PACKAGE_PIN D6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L12_P
#set_property PACKAGE_PIN D7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L23_N
#set_property PACKAGE_PIN E2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L23_P
#set_property PACKAGE_PIN F2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L4_N
#set_property PACKAGE_PIN A3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L4_P
#set_property PACKAGE_PIN B3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L5_N
#set_property PACKAGE_PIN B1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L5_P
#set_property PACKAGE_PIN C1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L6_N
#set_property PACKAGE_PIN F1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L6_P
#set_property PACKAGE_PIN G1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L13_N
#set_property PACKAGE_PIN C2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L13_P
#set_property PACKAGE_PIN C3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L24_N
#set_property PACKAGE_PIN D1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L24_P
#set_property PACKAGE_PIN E1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L9_N
#set_property PACKAGE_PIN C9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B66_L9_P
#set_property PACKAGE_PIN B9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L5_N
#set_property PACKAGE_PIN E14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L5_P
#set_property PACKAGE_PIN E13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L2_N
#set_property PACKAGE_PIN J14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L2_P
#set_property PACKAGE_PIN K14 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L3_N
#set_property PACKAGE_PIN A15 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L3_P
#set_property PACKAGE_PIN B15 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L6_N
#set_property PACKAGE_PIN F13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B47_L6_P
#set_property PACKAGE_PIN G13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
