# This constraint file was automatically generated 
 #based on the pinout Excel File. It is only valid for 4EG. 

#----- West Connector -----
# Carrier signal name B228_RX1_N
#set_property PACKAGE_PIN V1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_RX1_P
#set_property PACKAGE_PIN V2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_TX1_N
#set_property PACKAGE_PIN U3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B228_TX1_P
#set_property PACKAGE_PIN U4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_RX1_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_RX1_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_TX1_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B229_TX1_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_RX1_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_RX1_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_TX1_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B230_TX1_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_RX1_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_RX1_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_TX1_N
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B128_TX1_P
#set_property PACKAGE_PIN -- [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L11_N
#set_property PACKAGE_PIN AF5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L11_P
#set_property PACKAGE_PIN AE5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L14_N
#set_property PACKAGE_PIN AD4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L14_P
#set_property PACKAGE_PIN AD5 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L13_N
#set_property PACKAGE_PIN AC3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L13_P
#set_property PACKAGE_PIN AC4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L17_N
#set_property PACKAGE_PIN AH2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L17_P
#set_property PACKAGE_PIN AH1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L18_N
#set_property PACKAGE_PIN AF3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L18_P
#set_property PACKAGE_PIN AE3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L20_N
#set_property PACKAGE_PIN AF1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B64_L20_P
#set_property PACKAGE_PIN AG1 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L6_N
#set_property PACKAGE_PIN D10 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L6_P
#set_property PACKAGE_PIN E10 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L3_N
#set_property PACKAGE_PIN B10 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L3_P
#set_property PACKAGE_PIN C11 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L11_N
#set_property PACKAGE_PIN K3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L11_P
#set_property PACKAGE_PIN K4 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L7_N
#set_property PACKAGE_PIN Y8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L7_P
#set_property PACKAGE_PIN W8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L12_N
#set_property PACKAGE_PIN L2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L12_P
#set_property PACKAGE_PIN L3 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L15_N
#set_property PACKAGE_PIN R8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L15_P
#set_property PACKAGE_PIN T8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L18_N
#set_property PACKAGE_PIN K2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L18_P
#set_property PACKAGE_PIN J2 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L19_N
#set_property PACKAGE_PIN T6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L19_P
#set_property PACKAGE_PIN R6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L14_N
#set_property PACKAGE_PIN 0 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L14_P
#set_property PACKAGE_PIN 0 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L4_N
#set_property PACKAGE_PIN M8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L4_P
#set_property PACKAGE_PIN L8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L9_N
#set_property PACKAGE_PIN T7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L9_P
#set_property PACKAGE_PIN R7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L10_N
#set_property PACKAGE_PIN U8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L10_P
#set_property PACKAGE_PIN V8 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L13_N
#set_property PACKAGE_PIN L6 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L13_P
#set_property PACKAGE_PIN L7 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L6_N
#set_property PACKAGE_PIN V9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B65_L6_P
#set_property PACKAGE_PIN U9 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L4_N
#set_property PACKAGE_PIN C12 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L4_P
#set_property PACKAGE_PIN D12 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L1_N
#set_property PACKAGE_PIN K12 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L1_P
#set_property PACKAGE_PIN K13 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L2_N
#set_property PACKAGE_PIN J10 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L2_P
#set_property PACKAGE_PIN J11 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L5_N
#set_property PACKAGE_PIN F10 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
# Carrier signal name B48_L5_P
#set_property PACKAGE_PIN G11 [get_ports {Insert your signal name here}]
#set_property IOSTANDARD LVDS [get_ports {Insert your signal name here}]
#set_property DIFF_TERM_ADV TERM_100 [get_ports {Insert your signal name here}]
