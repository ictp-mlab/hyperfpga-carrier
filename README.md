# HyperFPGA-Carrier

This repository hosts the schematics, PCB and fabrication files for the HyperFPGA.

This board is a carrier for a SOM 5.2 x 7.6 cm. The carrier has 4 high density connectors [FX18-140P/S-0.8-SH](https://www.hirose.com/en/product/p/CL0579-0007-9-00).
This connectors will allow the board to be connected together as tiles. The connectors house several signals to allow high-speed communication. The SoC-FPGAs compatible with the carrier offer the following IOs.

| Type | B2B Connector | I/O Signals | LVDS Pairs |
| ---- | ------------- | ----------- | ---------- |
| HD   | J3            | 24 I/Os     | 12         |
| HD   | J3            | 24 I/Os     | 12         |
| HP   | J4            | 52 I/O's    | 24         |
| HP   | J4            | 52 I/Os     | 24         |
| HP   | J1            | 48 I/Os     | 24         |
| MIO  | J3            | 13 I/Os     | \-         |
| MIO  | J3            | 26 I/Os     | \-         |
| MIO  | J3            | 26 I/Os     | \-         |

The HP type pins can go as high as 625Mhz and the HD to 250Mhz. To allow a symmetrical implementation the HP and HD banks have been divided in groups of 6 LVDS pairs and routed to the connectors. The connectors are selected and positioned in a way that it is possible to interconnect several boards in all of the four directions.

It is intended to be used with the UDMA packet protocol.

![Mock-up](images/mockup.png)

The carrier is intended to work with the whole range of Trenz SOMS (TE0803, TE0807 and TE0808). This requires

## Changes

* Change the USB-UART bridge to a x1 chip (smaller, maybe cheaper).

Ignored.

* Distribute 16 GTH, 4 on each RA connector.

Done.

* Decide if JTAG needs to be on-board or just by pins.

Ignored for the moment given the difficulty it would pose when configuring for use.

* SFP might require I2C for control, this might be also routed from the PL.

Ignored.

* Should the PMODs be removed?

Done, reverted. A vertical PMOD was added housing the B66 and B64  T lines.

* Should the buttons and switches also be removed?

Nope, added one more button.

* Replace the SD + MMC multiplexer IC for a single SD IC.

To be discussed.

## Schematics

The schematic is based on the [MSADC-Carrier project](https://gl.ire.pw.edu.pl/compass-dsp/msadc_som_carrier). Likewise inspiration was taken from the schematic of the Trenz [TEBF0808-04A-carrier](https://shop.trenz-electronic.de/trenzdownloads/Trenz_Electronic/Modules_and_Module_Carriers/5.2x7.6/5.2x7.6_Carriers/TEBF0808/REV04/Documents/SCH-TEBF0808-04A.PDF) and the [Xilinx ZCU102 Development Board](https://www.xilinx.com/products/boards-and-kits/ek-u1-zcu102-g.html#documentation).

![Mock-up](images/top.png)

### To-do

- [x] Define components to be added and removed from the base design.
- [x] Determine the fan-out. It should take into account all SoMs and preferably include possible GTHs and GTRs.
- [x] Connect the PL and PS lines to the connectors.
- [x] Add the power monitoring ICs (SMBUS based, INA226).
- [x] Validate the connections (DRC checks, etc.).

### To review

- [X] Banks' power
- [x] I2C compatibility with oscillator (SI)
- [X] Mirror pins on footprint instead of schematic
- [X] Possibly add a 1x4 mux for I2C1
- [X] Check and test PS GPIOs banks
- [X] Check ETH
- [X] Change net names on connectors to facilitate follow up
- [X] Check tx-rx on all gth

### CAN 1

The external PHY was added. The CANH and CANL lines are redirected with jumpers. A 120ohm resistor can be added for the physical termination using a jumper.

## Pinout

The carrier supports TE0803, TE0807, and TE0808 SoMs.
Minor variations between the families are present.

| Resource   | Pins              | Interface             |
|------------|-------------------|-----------------------|
| QSPI       | MIO 0-12          | SOM SPI Flash         |
| PS GPIO 0  | MIO 0-25          |                       |
| PS GPIO 1  | MIO 26-51         |                       |
| PS GPIO 2  | MIO 52-77         |                       |
| PS SD 1    | MIO 46-51         | Carrier micro SD Card |
| PS CAN 1   | MIO 52-53         | Carrier CAN bus       |
| PS UART 0  | MIO 42-43         | Serial-USB converter  |
| PS UART 1  | MIO 56-57         | Serial-USB converter  |
| PS GEM 3   | MIO 64-75         | Gb Ethernet           |
| GPIO SW0-3 | MIO 19-22         | US0 switch            |
| GPIO SW4-7 | MIO 23-25, MIO 32 | US1 switch            |
| PCIe Reset | MIO 37            |                       |
| GPIO PB0   | MIO 34            | UB1                   |
| GPIO PB1   | MIO 33            | UB0                   |
| GPIO LED0  | MIO 35            | UL0                   |
| GPIO LED1  | MIO 36            | UL1                   |
| GPIO LED2  | MIO 44            | UL2                   |
| GPIO LED3  | MIO 62            | UL3                   |
| ETH_RST    | MIO 63            | ETH Phy reset         |
| PS I2C 0   | MIO 38-39         | SOM SI5338 addr. 0x70 |
| PLL RSTn    | MIO 58            | Only TE0807 and TE0808          |
| PLL LOLn   | MIO 59            | Only TE0807 and TE0808           |
| PLL SEL1   | MIO 60            | Only TE0807 and TE0808           |
| PLL SEL0   | MIO 61            | Only TE0807 and TE0808           |
