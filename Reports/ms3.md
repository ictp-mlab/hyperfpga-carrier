# MS3: Custom FSBL

This milestone is completely dedicated to understanding how to customize the FSBL for our Linux kernel.
There are two considered paths to achieve this.

## Patch the FSBL inside Petalinux

This path consists on creating a user recipe inside the Petalinux project.
Inside this recipe Petalinux is instructed to apply a git patch to the FSBL files.

## Vitis custom FSBL

In this case we will generate the FBLS in Vitis and then instruct Petalinux to use the generated ```fsbl.elf``` file.

We start assuming you already know how to create a custom platform in Vivado and export it with the bitstream.

1. Open Vitis.
2. Create a new Platform project.
3. Select the ```.xsa``` file containing your custom platform.
4. In **Operating System** specify Linux and click finish.
5. Select **Generate boot components**

A new platform project is generated in the current workspace.

1. Open the ```platform.spr``` file.
2. Select the top domain and check **Generate boot components**.
3. Open the linux domain and add the **Bif** file from the **image** folder of our Petalinux project.
4. Do the same for the **Boot Components Directory**.

At this point we can go to the **Project Explorer** and open the source files for our platform.

1. Select the files to modify the FSBL and PMU.
2. After building the platform the ```.elf``` files will be generated in the **export/\<platform project name\>/sw/\<platform project name\>/boot**.
3. These files will be referenced later when we package the Petalinux project.

Now lets go back to Petalinux.

1. To avoid the automatic generation of the FSBL we need to change the configuration of our project.
2. Open the project root folder and run ```petalinux-config```
3. Under **Linux Component Selection** deselect the **FSBL** and **PMU**.
4. Now run ```petalinux-build```.
5. Once its built, the project should be ready to be packaged with our custom **FSBL**.

## References

[Creating a BSP for PetaLinux](https://indico.cern.ch/event/952288/contributions/4033881/attachments/2116542/3561511/2020-10-06_Creating_a_BSP_for_PetaLinux.pdf)
