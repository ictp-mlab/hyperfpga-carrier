# MS2: Ubuntu minimal running from an SD card with network services and FPGA manager

After completing the steps of the [MS1: Minimal Uboot running from an SD card](ms1.md) we got a linux kernel running on the A53 quad-core of our ZinqMP.

1. Now to deploy an Ubuntu image we first need to download the corresponding version from this [link](https://rcn-ee.com/rootfs/eewiki/minfs/).
2. One we downloaded the desired linux distro we can proceed to extract it with the following command:

   ```tar xf <linux distro>.tar.xz```

3. This will create a new folder with the extracted content where we will find another ```tar``` file that contains the root file system we need to extract and copy on our SD card **RootFS** partition.
4. Lest clean the RootFS partition with the following command:

    ```sudo rm -rf RootFS/```

5. Copy the ```rootfs``` file from the extracted folder into the **RootFS** partition.

    ```sudo cp <linux distro>/armhf-rootfs-<distro>-<version>.tar <SD card mounting point>/RootFS/```

6. Now we can extract it:

    ```sudo tar xf armhf-rootfs-<distro>-<version>.tar```

7. You can remove the compressed file with the ```rm``` command.

8. Plug the SD card on your board, open a serial terminal and you should see Ubuntu, or whatever distro you chose boot up.

## Blinking LEDs

This is super simple but might be confusing at the begning. By defaul the kernel will load the zynqMP-gpio driver. The drivers are situated at:
    ```/sys/class/gpio```
Doing a simple ```ls``` on the console we can see which drivers are available. In this case the result is the following:

```root@arg:/sys/class/gpio# ls```

```export gpiochip334  gpiochip508 unexport```

These are two independent instances of our gpios. To check how the pins were effectively distributed we can run the following command:

```root@arg:/sys/class/gpio# cat gpiochip334/ngpio```

This command returns the value of ```174``` for ```gpiochip334``` and ```4``` for the ```gpiochip508```. These two values indicate the quantity of pins managed by each chip. To use them we must follow a series of steps.

1. Export the GPIO pin for usage. The **369** number corresponds to **GPIO35**. You can apply this offset to any GPIO to find the correct number for your GPIO.

    ```# echo 369 > /sys/class/gpio/export```

2. Now this folder will appear ```/sys/class/gpio/gpio369```. This is a symbolic folder with these files inside:

    ```active_low device direction edge power subsystem uevent value```

3. Each of this files performs the operation indicated in its name. Now lets set **GPIO35** as an output to drive a LED.

    ```# echo out > /sys/class/gpio/gpio369/direction```

4. By writing to the ```value``` file we can turn the LED on (1) or off (0).

## Troubleshooting

When performing an ```echo``` over this files you get ```Permission denied``` you can try the following fix:

<code>

sudo groupadd gpio

sudo usermod -aG gpio \<myusername>

su \<myusername>

sudo chgrp gpio /sys/class/gpio/export

sudo chgrp gpio /sys/class/gpio/unexport

sudo chmod 775 /sys/class/gpio/export

sudo chmod 775 /sys/class/gpio/unexport
</code>

When performing an ```echo 0``` you get ```Invalid argument```. It is highly probable that you are trying to export an invalid **GPIO**. Remember that the indexing of the pins starts with lowest value of the ```gpiochip```.

## References

[Raspberry pi Forum - GPIO: Permission Denied](https://forums.raspberrypi.com/viewtopic.php?t=5185)

[Supueruser - GPIO invalid argument](https://superuser.com/questions/1449923/trying-to-write-to-sys-class-gpio-export-write-error-invalid-argument)

## I2C Test

A simple way to check if the I2C is working is by performing an address scan. Before doing this we need to know the **ID** of our I2C bus. The following command displays the bus ID, driver used, and other info.

```# i2cdetect -l```

In my case the output is the following:

```i2c-0    i2c     Cadence I2C at ff02000      I2C adapter```

From these we can say that the bus **ID** is 0. By using this in the next command we can perform a complete address scan showing all connected devices.

![i2c](images/i2c.png)

Here we can see that the bus has two clients **0x50** and the SiLabs on-SOM oscillator at
**0x70**.

## CAN Bus Test

This one is way more tricky and we will cover it further ahead.

## FPGA Manager Test



