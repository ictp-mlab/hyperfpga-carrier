# MS1: Minimal Uboot running from an SD card

For this part the SD card must be enabled in the ZynqMP from the Vivado part.

1. Open Petalinux. Run the following command in your Petalinux installation folder

   ```$ source settings.sh```

2. Create a Petalinux project.

    ```$ petalinux-create --type project --name <project_name> --template zynqMP```

3. Now let's go inside the new folder named ```<project_name>``` which was created by the previous command.
4. Inside the folder we want to configure the hardware using our Vivado exported platfrom. We do this with the following command:

    ```<project_name> $ petalinux-configure --get-hw-description <path to the Vivado exported xsa>```

## Project Configuration

A configuration window will pop up. In here we can change several characteristics of the system.

1. The ```Linux Components Selection``` is essentially used to let petalinux generate the FSBL, PMU, and ps_init. If FSBL and PMU are disabled in this menu one must use Vitis to generate these files. For now lets leave them selected.

2. Enter ```DTG Settings -> Kernel Bootargs -> Auto generated bootargs``` it should look like this ```earlycon console=ttyPS0,115200 clk_ignore_unused root=/dev/mmcblk0p2 rw rootwait```.
   1. You should set your root cmdline argument to ```mmcblk0p<whichever partition is the rootfs>```. Only use mmcblk0 when booting from internal NAND.
3. Enable the ```FPGA manager```.
4. In ```Image Packaging Configuration -> Root filesystem type``` select ```EXT4 (SD/eMMC/SATA/USB)```.
5. In ```Image Packaging Configuration -> Device node of SD device``` should match the ```root``` specified in the ```Auto generated bootargs``` in step 2.
6. Press Escape twice and save the configuration.

## U-boot Configuration

1. Now let's configure U-boot by running the following command.

   ```<project_name> $ petalinux-configure -c u-boot```

2. Under ```Boot options -> Boot media``` enable ```Support for booting from SD/EMMC```
3. Press Escape twice and save the configuration.

## Configure Device Tree

This part is essential for SD boot of the file system.

1. We need to modify this file: ```<project_name>/project-spec/meta-user/recipes-bsp/device-tree/files/system-user-dtsi```.
2. Open it with any mean you prefer.
3. The entry for the SD must be modified with the following lines:

    ```rb
    disable-wp;
    no-1-8-v;
    ```

4. The final device tree file should show the SD entry as follows:

```rb
/include/ "system-conf.dtsi"
/{
};

&sdhci1 {
    no-1-8-v;
    disable-wp;
};
```

## Build and Package the Project

1. To build the project run the following command in the terminal:

    ```<project_name> $ petalinux-build```

2. Once this process is completed we can package the files using:

    ```<project_name> $ petalinux-package --boot --fsbl <images/linux/zynqmp_fsbl.elf> --pmufw <images/linux/pmufw.elf> --u-boot <images/linux/u-boot.elf> --boot-devices sd```

3. The previous command will generate the required files for boot that we need to copy into our SD card.

    ![package](images/package.png)

## Prepare the SD Card

This can be done using ```fdisk``` as explained here [Xilinx - How to format SD card for SD boot](https://xilinx-wiki.atlassian.net/wiki/spaces/A/pages/18842385/How+to+format+SD+card+for+SD+boot) or by using some visual tool like ```gparted```. I prefer simpler and more graphic tools.

1. Open ```gparted```.
2. Select the SD card from the drop down menu on the top right side.
3. Unmount all partitions by right clicking and selecting ```Unmount```.
4. Delete all partitions.
5. Create a new ```FAT32``` partition with **4MiB** of preceding Free Space and at least 500MiB in size. Label it as ```BOOT```.
6. Create a new ```EXT4``` partition with the rest of the volume. Label it as ```RootFS```.
7. Apply all operations. Your SD card should show up like this:

    ![sd card](images/sd_parts.png)

8. Proceed to copy the following files into the ```BOOT``` partition:

    ```rb
    BOOT.BIN
    image.ub
    boot.scr
    ```

9. Extract the ```rootfs.tar.gz``` folder into the RootFS ```ext4``` partition of the SD card with the following command:

    ```tar xzf rootfs.tar.gz```

10. Open a console on your PC and start the preferred serial communication program with the baud rate set to ```115200``` on that console.
11. Set the boot mode of the board to SD boot. **Refer to the board documentation for details.**
12. Plug the SD card into the board.
13. Power on the board.
14. After some seconds you should see the linux prompt in your serial console.

    ![boot](images/boot.png)

## Troubleshooting

Remember to install all Xilinx tools in an Ext4 partition.

Do not use sudo to install any of the tools (Vivado, Vitis, Petalinux).

If you don't see anything coming up on the Serial Terminal make sure you are connecting to the corresponding COM or TTY. Also, if you are using a USB hub remove it and connect the USB directly to the PC.
