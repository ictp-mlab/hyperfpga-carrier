# How to Boot Linux on the HyperFPGA

## List of content

## Objective

The purpose of this implementation is to allow users to deploy applications using a tftp server.
This eliminates the need of the JTAG and facilitates the configuration of the the single board when dealing with a cluster.
The process is simple and it will be slowly implemented to offer a stable platform.
To ensure stability several factors will be taken into account as the development progresses.

### Roadmap

- [x] [MS1: Minimal Uboot running from an SD card](ms1.md)
- [x] [MS2: Ubuntu minimal running from an SD card with network services and FPGA manager](ms2.md)
  - [x] Linux blinking LED, swithces and buttons.
  - [ ] FPGA PMOD test.
  - [x] I2C test
  - [x] CAN test
- [ ] [MS3: Custom FSBL](ms3.md)
- [ ] MS4: Network boot from a dedicated server and automatic FPGA bitstream configuration

Nice to have:

- Remove unnecessary drivers and boot Ubuntu minimal.
- Custom FSBL on the RPU for OpenAMP support

## Requirements

Given the different releases of all the required tools it is hard to future proof the process.
Thus, several changes will be needed for the correct functioning outside of the tested versions.

- Vivado 2021.2
- Vitis 2021.2
- Petalinux 2021.2
- Ubuntu 20.04 (or any version you want click [here for downloads](https://rcn-ee.com/rootfs/eewiki/minfs/))
- SD card with at lest 8GB
- Ubuntu minimal
- HyperFPGA carrier board with SOM

Most of the content is broadly covered in the following references:

[Whitney Knitter - hackster.io](https://www.hackster.io/whitney-knitter/installing-vivado-vitis-petalinux-2021-2-on-ubuntu-18-04-0d0fdf)

[Enes Göktaş - medium](https://enes-goktas.medium.com/install-and-run-ubuntu-20-04-on-zynq-7000-soc-zc706-evaluation-board-c5fef2423c98)

[Chathura Niroshan - medium](https://medium.com/developments-and-implementations-on-zynq-7000-ap/install-ubuntu-16-04-lts-on-zynq-zc702-using-petalinux-2016-4-e1da902eaff7)

[Xilinx](https://support.xilinx.com/s/article/73296?language=en_US)

[Trenz forum](https://forum.trenz-electronic.de/index.php?topic=1322.0)

## Process

### Create the custom hardware platform

1. Open Vivado. Run the following command in your Vivado installation folder

   ```$ source /opt/Xilinx/Vivado/2021.2/settings64.sh```

   ```$ vivado```

2. Create a new project for your SOM. (You can install the SOM from the Vivado Store)
3. Create a block design
4. Place the ZynqMP block
5. Configure the peripherals. (at least one UART, Memory device and Ethernet must be present).
6. Generate output products.
7. Create VHDL wrapper.
8. Export hardware (only if no extra blocks are present in the block design).

### Create the bootables

The steps in this section will depend on the MS.
